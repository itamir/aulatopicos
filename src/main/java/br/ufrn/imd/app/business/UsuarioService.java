package br.ufrn.imd.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.ufrn.imd.app.dao.UsuarioDao;
import br.ufrn.imd.app.model.Usuario;

@Stateless
public class UsuarioService {
	
	@Inject
	private UsuarioDao usuarioDao;
	
	public Usuario salvarOuAtualizar(Usuario usuario) {
		if(usuario.getLogin() != null)
			usuarioDao.salvarOuAtualizar(usuario);
		
		return usuario;
	}
	
	public List<Usuario> listagem(){
		return usuarioDao.listagem();
	}
}
