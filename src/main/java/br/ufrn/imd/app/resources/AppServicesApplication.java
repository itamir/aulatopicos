package br.ufrn.imd.app.resources;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/services")
public class AppServicesApplication extends Application{
 
}